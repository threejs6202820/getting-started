import { GLTFLoader } from "three/addons/loaders/GLTFLoader.js";
import AbstractModelLoaderService from "./AbstractModelLoaderService.js";

class GLTFService extends AbstractModelLoaderService {
  constructor(modelPath, { onLoad, onError }) {
    super(modelPath, { onLoad, onError });
  }

  load() {
    const filePath = this.getModelFilePath();
    const onLoadCallback = this.getLoadCallback();
    const onErrorCallback = this.getErrorCallback();

    const loader = this.getLoader();

    loader.load(filePath, onLoadCallback, undefined, onErrorCallback);
  }
  
  createLoader() {
    return new GLTFLoader();
  }
}

export default GLTFService;
