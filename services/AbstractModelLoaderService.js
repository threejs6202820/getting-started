class AbstractModelLoaderService {
  constructor(modelPath, { onLoad, onError }) {
    this.modelPath = this.setModelFilePath(modelPath);
    this.onLoad = this.setLoadCallback(onLoad);
    this.onError = this.setErrorCallback(onError);
    this.loader = this.setLoader(this.createLoader());
  }

  // abstract

  load() {}

  createLoader() {}

  setLoader(loader) {
    this.loader = loader;
    return this.loader;
  }

  setModelFilePath(modelPath) {
    this.modelPath = modelPath;
    return this.modelPath;
  }

  setErrorCallback(callback) {
    this.onError = callback;
    return this.onError;
  }

  setLoadCallback(callback) {
    this.onLoad = callback;
    return this.onLoad;
  }

  getLoader() {
    return this.loader;
  }

  getModelFilePath() {
    return this.modelPath;
  }

  getErrorCallback() {
    return this.onError;
  }

  getLoadCallback() {
    return this.onLoad;
  }
}

export default AbstractModelLoaderService;
