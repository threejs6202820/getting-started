import AbstractModel from "../AbstractModel.js";

class ArmStretchingBot extends AbstractModel {
  constructor(gltf) {
    super(gltf);
  }

  animateModel(renderer, scene, camera, controls) {
    requestAnimationFrame(
      this.animateModel.bind(this, renderer, scene, camera, controls)
    );

    controls.forEach(control => {
      control.onUpdateControls();
    });

    renderer.render(scene, camera);
  }
}

export default ArmStretchingBot;
