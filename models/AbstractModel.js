class AbstractModel {

    constructor(gltf) {
      this.model = this.setModel(gltf);
    }

    // abstract
    animateModel(renderer, scene, camera) {}

    setModel(model){
      this.model = model;
      return model;
    }

    getModel(){
      return this.model;
    }

    setPosition(position){
      const model = this.getModel();
      model.scene.position.set(position.x, position.y, position.z);
      
      return model;
    }
  
  }
  
  export default AbstractModel;
  