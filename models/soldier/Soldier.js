import * as THREE from "three";
import AbstractModel from "../AbstractModel.js";
import ModelControls from "../../controls/ModelControls.js";
import SoldierControls from "./SoldierControls.js";

const clock = new THREE.Clock();

class Soldier extends AbstractModel {
  gltfAnimations = [];
  keysPressed = {};
  mixer;
  modelControls = new ModelControls();
  solderControls;

  constructor(gltf) {
    super(gltf);

    this.setGltfAnimations(gltf.animations);
  }

  animateModel(renderer, scene, camera, controls) {
    const solderControls = this.getSolderControls();
    const keysPressed = this.getKeysPressed();

    let mixerUpdateDelta = clock.getDelta();

    if (solderControls) {
      solderControls.update(mixerUpdateDelta, keysPressed);
    }

    requestAnimationFrame(
      this.animateModel.bind(this, renderer, scene, camera, controls)
    );

    controls.forEach((control) => {
      control.onUpdateControls();
    });

    renderer.render(scene, camera);
  }

  attachModelControls(controls, camera) {
    const model = this.getModel();
    const mixer = this.getMixer();
    const gltfAnimations = this.getGltfAnimations();

    this.setSolderControls(
      new SoldierControls(
        model.scene,
        mixer,
        gltfAnimations,
        controls,
        camera,
        "Idle"
      )
    );

    this.attachModelKeyDown();
    this.attachModelKeyUp();

    this.modelControls.addControls(this.solderControls);
  }

  attachModelKeyDown() {
    const modelControls = this.getModelControls();
    const solderControls = this.getSolderControls();

    modelControls.onKeyDown((event) => {
      if (event.shiftKey && solderControls) {
        // TODO: not working
        solderControls.switchRunToggle();
      } else {
        const keyPressed = event.key.toLowerCase();
        this.setKeysPressed(keyPressed, true);
      }
    });
  }

  attachModelKeyUp() {
    const modelControls = this.getModelControls();
    modelControls.onKeyUp((event) => {
      const keyPressed = event.key.toLowerCase();
      this.setKeysPressed(keyPressed, false);
    });
  }

  onTraverse() {
    const model = this.getModel();

    model.scene.traverse((object) => {
      if (object.isMesh) {
        object.castShadow = true;
      }
    });
  }

  getGltfAnimations() {
    return this.gltfAnimations;
  }

  getKeysPressed() {
    return this.keysPressed;
  }

  getMixer() {
    return this.mixer;
  }

  getModelControls() {
    return this.modelControls;
  }

  getSolderControls() {
    return this.solderControls;
  }

  setGltfAnimations(animations) {
    const model = this.getModel();

    const mixer = new THREE.AnimationMixer(model.scene);
    const animationsMap = new Map();

    animations
      .filter((a) => a.name != "TPose")
      .forEach((a) => {
        animationsMap.set(a.name, mixer.clipAction(a));
      });

    this.gltfAnimations = animationsMap;
    this.setMixer(mixer);
  }

  setKeysPressed(keyPressed, state) {
    this.keysPressed[keyPressed] = state;
  }

  setMixer(mixer) {
    this.mixer = mixer;
  }

  setModelControls(modelControls) {
    this.modelControls = modelControls;
  }

  setSolderControls(solderControls) {
    this.solderControls = solderControls;
  }
}

export default Soldier;
