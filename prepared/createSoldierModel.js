import ThreeJSField from "../field/ThreeJSField.js";
import GLTFService from "../services/GLTFService.js";
import Soldier from "../models/soldier/Soldier.js";
import DirectionalLight from "../parts/DirectionalLight.js";
import Floor from "../parts/Floor.js";

function createSoldierModel() {
  const cammeraArgs = {
    fov: 45,
    aspectRatio: window.innerWidth / window.innerHeight,
    near: 0.1,
    far: 1000,
    position: {
      x: 0,
      y: 5,
      z: 5,
    },
  };

  const rendererArgs = {
    size: {
      width: window.innerWidth,
      height: window.innerHeight,
    },
  };

  const threeField = new ThreeJSField({
    camera: cammeraArgs,
    renderer: rendererArgs,
    orbitControls: true,
  });

  const directionalLight = new DirectionalLight(0xffffff, 300);
  directionalLight.setPosition({ x: 0, y: 0, z: 0 });

  const floorInstance = new Floor([]);

  const onLoadHanlder = (gltf, position, Model) => {
    console.log('Solder model on load');

    const sceneInstance = threeField.getSceneInstance();
    const cameraInstance = threeField.getCameraInstance();
    const rendererInstance = threeField.getRendererInstance();
    const orbitControlsInstance = threeField.getOrbitControlsInstance();

    const model = new Model(gltf);
    const object = model.getModel().scene;

    model.setPosition(position);

    sceneInstance.addElementToScene(object);
    sceneInstance.addElementToScene(directionalLight.getLightInstance());
    sceneInstance.addElementToScene(floorInstance.getFloorInstance());

    const controls = [orbitControlsInstance];

    model.attachModelControls(controls, cameraInstance.camera);

    model.animateModel(
      rendererInstance.renderer,
      sceneInstance.scene,
      cameraInstance.camera,
      controls
    );
  };

  const onErrorHanlder = (error) => {
    console.log("error args", error);
  };

  const gltfService = new GLTFService("/models/soldier/Soldier.glb", {
    onLoad: (gltf) => onLoadHanlder(gltf, { x: 0, y: 0, z: 0 }, Soldier),
    onError: onErrorHanlder,
  });

  gltfService.load();
}

export default createSoldierModel;
