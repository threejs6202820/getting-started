import ThreeJSField from "../field/ThreeJSField.js";
import GLTFService from "../services/GLTFService.js";
import WindTurbine from "../models/wind-turbine/WindTurbine.js";
import DirectionalLight from "../parts/DirectionalLight.js";

function create3DModel() {
  const cammeraArgs = {
    fov: 75,
    aspectRatio: window.innerWidth / window.innerHeight,
    near: 2,
    far: 500,
    position: {
      x: 0,
      y: 0,
      z: 10,
    },
  };

  const rendererArgs = {
    size: {
      width: window.innerWidth,
      height: window.innerHeight,
    },
  };

  const threeField = new ThreeJSField({
    camera: cammeraArgs,
    renderer: rendererArgs,
    orbitControls: true,
  });

  const directionalLight = new DirectionalLight(0xffffff, 300);
  directionalLight.setPosition({ x: 0, y: 0, z: 0 });

  const onLoadHanlder = (gltf, position, Model) => {
    const sceneInstance = threeField.getSceneInstance();
    const cameraInstance = threeField.getCameraInstance();
    const rendererInstance = threeField.getRendererInstance();
    const orbitControlsInstance = threeField.getOrbitControlsInstance();

    const windTurbineModel = new Model(gltf);
    const windTurbine = windTurbineModel.getModel().scene;

    windTurbineModel.setPosition(position);

    sceneInstance.addElementToScene(windTurbine);
    sceneInstance.addElementToScene(directionalLight.getLightInstance());

    const controls = [orbitControlsInstance];

    windTurbineModel.animateModel(
      rendererInstance.renderer,
      sceneInstance.scene,
      cameraInstance.camera,
      controls
    );
  };

  const onErrorHanlder = (error) => {
    console.log("error args", error);
  };

  const gltfService = new GLTFService("/models/wind-turbine/Wind Turbine.glb", {
    onLoad: (gltf) => onLoadHanlder(gltf, { x: 0, y: 0, z: 0 }, WindTurbine),
    onError: onErrorHanlder,
  });

  gltfService.load();
}

export default create3DModel;
