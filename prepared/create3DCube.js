import ThreeJSField from "../field/ThreeJSField.js";
import Cube from "../shapes/Cube.js";

function create3DCube() {
    // typeof ThreeJSFieldArgs
    const cubeCammeraArgs = {
      fov: 75,
      aspectRatio: window.innerWidth / window.innerHeight,
      near: 0.1,
      far: 1000,
      position: {
        x: 0,
        y: 0,
        z: 5,
      },
    };
  
    const cubeRendererArgs = {
      size: {
        width: window.innerWidth,
        height: window.innerHeight,
      },
    };
  
    const threeField = new ThreeJSField({
      camera: cubeCammeraArgs,
      renderer: cubeRendererArgs,
    });
    const sceneInstance = threeField.getSceneInstance();
    const cameraInstance = threeField.getCameraInstance();
    const rendererInstance = threeField.getRendererInstance();
  
    const cube = new Cube();
    const cubeSceneInstance = cube.getElementForScene();
  
    sceneInstance.addElementToScene(cubeSceneInstance);
  
    cube.animateCube(
      rendererInstance.renderer,
      sceneInstance.scene,
      cameraInstance.camera
    );
  
    return cube;
}

export default create3DCube;