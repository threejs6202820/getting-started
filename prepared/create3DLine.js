import * as THREE from "three";
import ThreeJSField from "../field/ThreeJSField.js";
import Line from "../shapes/Line.js";

function create3DLine() {
  // typeof ThreeJSFieldArgs
  const lineCammeraArgs = {
    fov: 45,
    aspectRatio: window.innerWidth / window.innerHeight,
    near: 1,
    far: 500,
    position: {
      x: 0,
      y: 0,
      z: 100,
    },
    lookAt: [0, 0, 0],
  };

  const lineRendererArgs = {
    size: {
      width: window.innerWidth,
      height: window.innerHeight,
    },
  };

  const threeField = new ThreeJSField({
    camera: lineCammeraArgs,
    renderer: lineRendererArgs,
  });
  const sceneInstance = threeField.getSceneInstance();
  const cameraInstance = threeField.getCameraInstance();
  const rendererInstance = threeField.getRendererInstance();

  const linePoints = [
    new THREE.Vector3(-10, 0, 0),
    new THREE.Vector3(0, 10, 0),
    new THREE.Vector3(10, 0, 0),
  ];

  const line = new Line(linePoints);
  const lineSceneInstance = line.getElementForScene();

  sceneInstance.addElementToScene(lineSceneInstance);

  rendererInstance.renderer.render(sceneInstance.scene, cameraInstance.camera);

  return line;
}

export default create3DLine;
