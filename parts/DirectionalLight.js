import * as THREE from "three";

class DirectionalLight{
    constructor(color, intensity){
        this.light = this.setLigthInstance(this.createLight());
        this.color = color
        this.intensity = intensity;
    }

    getLightInstance(){
        return this.light;
    }

    getLightColor(){
        return this.color;
    }

    getLigthIntensity(){
        return this.intensity
    }

    setLigthInstance(light){
        this.light = light;
        return light
    }

    setLightColor(color){
        this.color = color
        return this.color;
    }

    setPosition(position){
        const light = this.getLightInstance();
        light.position.set(position.x, position.y, position.z).normalize();

        return light;
    }

    createLight(){
        const color = this.getLightColor();
        const intensity = this.getLigthIntensity();
        return new THREE.AmbientLight(color, intensity);
    }
}

export default DirectionalLight;