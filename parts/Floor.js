import * as THREE from "three";

class Floor {
  // TEXTURES
  floorInstance;
  textureLoader = new THREE.TextureLoader();

  constructor(textures, width = 80, height = 80) {
    const textureLoader = this.getTextureLoader();
    this.setTextures(textures);

    const placeholder = textureLoader.load(
      "../textures/placeholder/placeholder.png"
    );
    const sandBaseColor = textureLoader.load(
      "../textures/sand/Sand 002_COLOR.jpg"
    );
    const sandNormalMap = textureLoader.load(
      "../textures/sand/Sand 002_NRM.jpg"
    );
    const sandHeightMap = textureLoader.load(
      "../textures/sand/Sand 002_DISP.jpg"
    );
    const sandAmbientOcclusion = textureLoader.load(
      "../textures/sand/Sand 002_OCC.jpg"
    );

    const geometry = new THREE.PlaneGeometry(width, height, 512, 512);
    const material = new THREE.MeshStandardMaterial({
      map: sandBaseColor,
      normalMap: sandNormalMap,
      displacementMap: sandHeightMap,
      displacementScale: 0.1,
      aoMap: sandAmbientOcclusion,
    });

    this.wrapAndRepeatTexture(material.map);
    this.wrapAndRepeatTexture(material.normalMap);
    this.wrapAndRepeatTexture(material.displacementMap);
    this.wrapAndRepeatTexture(material.aoMap);

    const floor = new THREE.Mesh(geometry, material);
    floor.receiveShadow = true;
    floor.rotation.x = -Math.PI / 2;

    this.setFloor(floor);
  }

  wrapAndRepeatTexture(map) {
    map.wrapS = map.wrapT = THREE.RepeatWrapping;
    map.repeat.x = map.repeat.y = 10;
  }

  getFloorInstance() {
    return this.floorInstance;
  }

  getTextureLoader() {
    return this.textureLoader;
  }

  getTextures() {
    return this.textures;
  }

  setFloor(floor){
    this.floorInstance = floor;
  }

  setTextureLoader(textureLoader) {
    this.textureLoader = textureLoader;
  }

  setTextures(textures) {
    this.textures = textures;
  }
}

export default Floor;
