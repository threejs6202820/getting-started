import * as THREE from "three";

class Scene {
  /**
   * @returns {THREE.Scene}
   */

  constructor() {
    this.scene = this.setSceneInstance(this.createScene());
  }

  setSceneInstance(scene) {
    this.scene = scene;
    return scene;
  }

  setLight(light){
    const sceneInstance = this.getSceneInstance();
    sceneInstance.scene.add(light);
    return sceneInstance;
  }

  setFloor(floor){
    const sceneInstance = this.getSceneInstance();
    sceneInstance.scene.add(floor);
    return sceneInstance;
  }

  getSceneInstance() {
    return this.scene;
  }

  createScene() {
    return new THREE.Scene();
  }

  addElementToScene(element) {
    const sceneInstance = this.getSceneInstance();
    sceneInstance.add(element);
  }
}

export default Scene;