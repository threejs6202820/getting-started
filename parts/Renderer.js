import * as THREE from "three";

// Interface IRenderer {
//     size: {
//         width: number;
//         height: number;
//      }
// }

class Renderer {
  /**
   * @returns {THREE.WebGLRenderer}
   */

  constructor(argsRenderer) {
    this.setSettingsInstance(argsRenderer);

    this.renderer = this.setRendererInstance(this.createRenderer());

    this.setRendererSettings();
  }

  setRendererInstance(renderer) {
    this.renderer = renderer;
    return renderer;
  }

  setRendererSettings() {
    const settings = this.getSettingsInstance();

    if(settings === void 0) return;

    this.setRendererSize(settings);
  }

  setRendererSize({size}) {
    /**
     * Note: If you wish to keep the size of your app but render it at a lower resolution,
     * you can do so by calling setSize with false as updateStyle (the third argument).
     * For example, setSize(window.innerWidth/2, window.innerHeight/2, false) will render your app at half resolution,
     * given that your <canvas> has 100% width and height.
     */
    if(size === void 0) return;
    
    const {width, height} = size;
    const renderer = this.getRendererInstance();

    if(width === void 0 || height === void 0) {
      renderer.setSize(window.innerWidth, window.innerHeight);
    }else{
      renderer.setSize(width, height);
    }

    return renderer;
  }

  setSettingsInstance(settings){
    this.settings = settings;
    return settings;
  }

  getRendererInstance() {
    return this.renderer;
  }

  getSettingsInstance(){
    return this.settings;
  }

  createRenderer() {
    return new THREE.WebGLRenderer();
  }

  appendChild(parentNode, childNode) {
    parentNode.appendChild(childNode);
  }
}

export default Renderer;