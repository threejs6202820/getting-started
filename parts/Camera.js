import * as THREE from "three";

// Interface ICamera {
//     fov: number;
//     aspectRatio: number;
//     near: number;
//     far: number;
//     position: {
//         x: number;
//         y: number;
//         z: number;
//     },
//     lookAt: number[]
// }

class Camera {
  /**
   * @param args ICamera
   * @returns {THREE.Camera}
   */

  constructor(argsCamera) {
    this.setSettingsInstance(argsCamera);

    this.camera = this.setCameraInstance(this.createCamera());

    this.setCameraSettings();
  }

  setCameraInstance(camera) {
    this.camera = camera;
    return camera;
  }

  setCameraSettings() {
    const { position, lookAt } = this.getSettingsInstance();

    if (position === void 0) {
      console.warn("Camera position is not defined");
    }

    position && this.setCameraPosition(position);
    lookAt && this.setCameraLookAt(lookAt);
  }

  setSettingsInstance(settings){
    this.settings = settings;
    return settings;
  }

  setCameraPosition(position) {
    if(position.x) this.setCameraPositionX(position.x);
    if(position.y) this.setCameraPositionY(position.y);
    if(position.z) this.setCameraPositionZ(position.z);
  }

  setCameraPositionX(x) {
    const camera = this.getCameraInstance();

    if (x !== void 0) camera.position.x = x;
  }

  setCameraPositionY(y) {
    const camera = this.getCameraInstance();

    if (y !== void 0) camera.position.y = y;
  }

  /**
   * By default, when we call scene.add(), the thing we add will be added to the coordinates (0,0,0).
   * This would cause both the camera and the cube to be inside each other.
   * To avoid this, we simply move the camera out a bit.
   *
   * @param {THREE.PerspectiveCamera} camera
   */
  setCameraPositionZ(z) {
    const camera = this.getCameraInstance();

    if (z !== void 0) camera.position.z = z;
  }

  setCameraLookAt(lookAt) {
    const camera = this.getCameraInstance();

    if (lookAt !== void 0) camera.lookAt(...lookAt);
  }

  getCameraInstance() {
    return this.camera;
  }

  getSettingsInstance(){
    return this.settings;
  }

  createCamera() {
    const settings = this.getSettingsInstance();
    // FOV е степента на сцената, която се вижда на дисплея във всеки даден момент. Стойността е в градуси.
    const fov = settings.fov;
    // Почти винаги искате да използвате ширината на елемента, разделена на височината, или ще получите същия резултат,
    // както когато пускате стари филми на широкоекранен телевизор - изображението изглежда смачкано.
    const aspectRatio = settings.aspectRatio

    // Това означава, че обекти, които са по-далеч от камерата от стойността на далеч или по-близо от близо, няма да бъдат изобразени.
    // Не е нужно да се притеснявате за това сега, но може да искате да използвате други стойности в приложенията си, за да получите по-добра производителност.
    const near = settings.near
    const far = settings.far;

    return new THREE.PerspectiveCamera(fov, aspectRatio, near, far);
  }
}

export default Camera;