import AbstractControls from "./AbstractControls.js";

class ModelControls extends AbstractControls {
  subsciptions = new Map([]);

  constructor() {
    super();
    this.addControls();
  }

  addControls() {
    document.addEventListener(
      "keydown",
      (event) => {
        const subsciptions = this.getSubsciptions();
        const keyDownSubsciptions = subsciptions.get("keydown");

        if (keyDownSubsciptions !== void 0) {
          keyDownSubsciptions.forEach(callback => {
            if(callback instanceof Function){
              return callback(event);
            }
          });
        }
      },
      false
    );

    document.addEventListener(
      "keyup",
      (event) => {
        const subsciptions = this.getSubsciptions();
        const keyDownSubsciptions = subsciptions.get("keyup");

        if (keyDownSubsciptions !== void 0) {
          keyDownSubsciptions.forEach(callback => {
            if(callback instanceof Function){
              return callback(event);
            }
          });
        }
      },
      false
    );
  }

  onKeyDown(callback) {
    const KEY = "keydown";
    const subsciptions = this.getSubsciptions();
    const keyDownSubsciptions = subsciptions.get(KEY);

    const updatedSubsciptions =
      keyDownSubsciptions !== void 0
        ? [...keyDownSubsciptions, callback]
        : [callback];

    this.setSubsciption(KEY, updatedSubsciptions);
  }

  onKeyUp(callback) {
    const KEY = "keyup";
    const subsciptions = this.getSubsciptions();
    const keyDownSubsciptions = subsciptions.get(KEY);

    const updatedSubsciptions =
      keyDownSubsciptions !== void 0
        ? [...keyDownSubsciptions, callback]
        : [callback];

    this.setSubsciption(KEY, updatedSubsciptions);
  }

  getSubsciptions() {
    return this.subsciptions;
  }

  /**
   * Set a subscription with a key and value.
   *
   * @param {type} key - The key for the subscription
   * @param {type} value - The value associated with the key
   * @return {type} description of return value
   */
  setSubsciption(key, value) {
    this.subsciptions.set(key, value);
  }
}

export default ModelControls;
