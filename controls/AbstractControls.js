class AbstractControls {
  createControls() {}

  onUpdateControls() {
    const controls = this.getControlsInstance();
    controls.update();

    return controls;
  }

  getControlsInstance() {
    return this.controls;
  }

  setControlsInstance(controls) {
    this.controls = controls;
    return this.controls;
  }
}

export default AbstractControls;
