import { OrbitControls } from "three/addons/controls/OrbitControls.js";
import AbstractControls from "./AbstractControls.js";

class OrbitControlsService extends AbstractControls {
  constructor(camera, renderer) {
    super();
    this.controls = this.setControlsInstance(
      this.createControls(camera, renderer)
    );
  }

  createControls(camera, renderer) {
    return new OrbitControls(camera, renderer.domElement);
  }
}

export default OrbitControlsService;
