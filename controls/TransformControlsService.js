import { TransformControls } from "three/addons/controls/TransformControls.js";
import AbstractControls from "./AbstractControls.js";

class TransformControlsService extends AbstractControls {
  constructor(camera, renderer) {
    super();
    this.controls = this.setControlsInstance(
      this.createControls(camera, renderer)
    );
  }

  attachModel(model) {
    const controls = this.getControlsInstance();
    controls.attach(model);

    return controls;
  }

  createControls(camera, renderer) {
    return new TransformControls(camera, renderer.domElement);
  }

  setTransformMode(mode) {
    const controls = this.getControlsInstance();
    controls.setMode(mode);

    return controls;
  }

  onUpdateControls() {
    const controls = this.getControlsInstance();
    return controls;
  }
}

export default TransformControlsService;
