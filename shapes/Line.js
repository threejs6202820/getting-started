import * as THREE from "three";
import AbstractElement from "./AbstactElement.js";

class Line extends AbstractElement {
  constructor(points) {
    super();
    this.line = null;

    this.setLinePoints(points);
  }

  getElementForScene() {
    const geometry = this.setLinePointsToBufferGeometry();
    const material = this.getMaterial();

    this.line = this.getElement(geometry, material);

    // Add to scene

    return this.line;
  }

  /**
   * To create a line, we need a BoxGeometry.
   * This is an object that contains all the points (vertices) and fill (faces) of the line. We'll explore this more in the future.
   * @returns {THREE.BoxGeometry}
   */
  getGeometry() {
    return new THREE.BufferGeometry();
  }

  /**
   * Next thing we will do is define a material. 
   * For lines we have to use LineBasicMaterial or LineDashedMaterial.
   * @returns {THREE.LineBasicMaterial}
   */
  getMaterial() {
    const blue = 0x0000ff;
    return this.setLineMeshInstance(
      new THREE.LineBasicMaterial({ color: blue })
    );
  }

  /**
   * A mesh is an object that takes a geometry, and applies a material to it, which we then can insert to our scene, and move freely around.
   * @param {THREE.BoxGeometry} geometry
   * @param {THREE.MeshBasicMaterial} material
   * @returns {THREE.Line}
   */
  getElement(geometry, material) {
    return new THREE.Line( geometry, material );
  }

  getLineMeshInstance() {
    return this.line;
  }

  getLinePoints(){
    return this.points
  }

  setLineMeshInstance(line) {
    this.line = line;
    return line;
  }

  setLinePoints(points) {
    this.points = points;
    return points;
  }

  /**
   * We will need a geometry with some vertices
   */
  setLinePointsToBufferGeometry() {
    const geometry = this.getGeometry();
    const points = this.getLinePoints();

    geometry.setFromPoints(points);

    return geometry;
  }
}

export default Line;
