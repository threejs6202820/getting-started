import * as THREE from "three";
import AbstractElement from "./AbstactElement.js";

class Cube extends AbstractElement {
  constructor() {
    super();
    this.cube = null;
  }

  getElementForScene() {
    const geometry = this.getGeometry();
    const material = this.getMaterial();

    this.cube = this.getElement(geometry, material);

    return this.cube;
  }

  /**
   * To create a cube, we need a BoxGeometry.
   * This is an object that contains all the points (vertices) and fill (faces) of the cube. We'll explore this more in the future.
   * @returns {THREE.BoxGeometry}
   */
  getGeometry() {
    return new THREE.BoxGeometry(1, 1, 1);
  }

  /**
   * In addition to the geometry, we need a material to color it.
   * For now, let's use a MeshBasicMaterial.
   * To keep things very simple, we only supply a color attribute of 0x00ff00, which is green.
   * This works the same way that colors work in CSS or Photoshop (hex colors).
   * @returns {THREE.MeshBasicMaterial}
   */
  getMaterial() {
    const green = 0x00ff00;
    return this.setCubeMeshInstance(
      new THREE.MeshBasicMaterial({ color: green })
    );
  }

  /**
   * A mesh is an object that takes a geometry, and applies a material to it, which we then can insert to our scene, and move freely around.
   * @param {THREE.BoxGeometry} geometry
   * @param {THREE.MeshBasicMaterial} material
   * @returns {THREE.Mesh}
   */
  getElement(geometry, material) {
    return new THREE.Mesh(geometry, material);
  }

  getCubeMeshInstance() {
    return this.cube;
  }

  setCubeMeshInstance(cube) {
    this.cube = cube;
    return cube;
  }

  /**
   * To see result, we need what's called a render or animate loop.
   * @param {THREE.WebGLRenderer} renderer
   * @param {THREE.Scene} scene
   * @param {THREE.PerspectiveCamera} camera
   */
  animateCube(renderer, scene, camera) {
    this.rotating({ x: 0.01, y: 0.01 });

    requestAnimationFrame(() =>
      this.animateCube(renderer, scene, camera)
    );

    renderer.render(scene, camera);
  }

  rotating(position) {
    if ("y" in position) {
      this.cube.rotation.x += position.x;
    }

    if ("y" in position) {
      this.cube.rotation.y += position.y;
    }

    if ("z" in position) {
      this.cube.rotation.z += position.z;
    }
  }
}

export default Cube;
