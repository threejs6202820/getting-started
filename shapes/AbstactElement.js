class AbstractElement {

  getGeometry(){}

  getMaterial(){}

  getElement(){}

  getElementForScene(){}

}

export default AbstractElement;
