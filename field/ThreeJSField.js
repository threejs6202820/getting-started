import Scene from "../parts/Scene.js";
import Camera from "../parts/Camera.js";
import Renderer from "../parts/Renderer.js";
import OrbitControlsService from "../controls/OrbitControlsService.js";
import TransformControlsService from "../controls/TransformControlsService.js";

// interface ThreeJSFieldArgs {
//     camera: ICamera;
//     renderer: IRenderer;
// }

class ThreeJSField {

    /**
     * @param args ThreeJSFieldArgs
     */
  constructor(args) {
    this.scene = this.setSceneInstance(new Scene());
    this.camera = this.setCameraInstance(new Camera(args.camera));
    this.renderer = this.setRendererInstance(new Renderer(args.renderer));

    if(args.orbitControls) {
      this.orbitControls = this.setOrbitControlsInstance(this.createOrbitControls());
    }

    if(args.transformControls) {
      this.transformControls = this.setTransformControlsInstance(this.createTransformControls());
    }

    this.renderElement(document.body, this.getRendererInstance().renderer.domElement);
  }

  setSceneInstance(scene) {
    this.scene = scene;
    return this.scene;
  }

  setCameraInstance(camera) {
    this.camera = camera;
    return this.camera;
  }

  setRendererInstance(renderer) {
    this.renderer = renderer;
    return this.renderer;
  }

  setOrbitControlsInstance(controls) {
    this.orbitControls = controls;
    return this.orbitControls;
  }

  setTransformControlsInstance(controls) {
    this.transformControls = controls;
    return this.transformControls;
  }


  getSceneInstance() {
    return this.scene;
  }

  getCameraInstance() {
    return this.camera;
  }

  getRendererInstance() {
    return this.renderer;
  }

  getOrbitControlsInstance() {
    return this.orbitControls;
  }

  getTransformControlsInstance() {
    return this.transformControls;
  }

  createOrbitControls() {
    const cameraInstance = this.getCameraInstance();
    const rendererInstance = this.getRendererInstance();

    return new OrbitControlsService(cameraInstance.camera, rendererInstance.renderer);
  }

  createTransformControls() {
    const cameraInstance = this.getCameraInstance();
    const rendererInstance = this.getRendererInstance();

    return new TransformControlsService(cameraInstance.camera, rendererInstance.renderer);
  }

  renderElement(parentNode, childNode) {
    const rendererInstance = this.getRendererInstance();

    rendererInstance.appendChild(parentNode, childNode);
  }
}

export default ThreeJSField;
