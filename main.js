// import create3DCube from "./prepared/create3DCube.js";
// import create3DLine from "./prepared/create3DLine.js";
// import create3DModel from "./prepared/create3DModel.js";
import createSoldierModel from "./prepared/createSoldierModel.js";


/**
 * Reference: https://threejs.org/docs/#manual/en/introduction/Creating-a-scene
 */

/**
 * To actually be able to display anything with three.js,
 * we need three things: scene, camera and renderer, so that we can render the scene with camera.
 */
function init() {
  // create3DCube();
  // create3DLine();
  // create3DModel();
  createSoldierModel();
}

init();
